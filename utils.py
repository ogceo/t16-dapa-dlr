import os
import requests
import mimetypes

class DAPAProvider():
    
    def __init__(self, name, base_url, result_dir='./responses'):
        self.mime = mimetypes.MimeTypes()
        self.name = name
        self.base_url = base_url
        self.result_dir = result_dir
        if not os.path.exists(result_dir):
            os.makedirs(result_dir)

    def request3(self):
        print(self.name)
        
    def request2(self, params):
        print(params)
                
    def request(self, path, output, params=None, headers=None, overwrite=True):
        print('###############')
        # construct URL
        pretty_path="[{}]   {}".format(self.name, path)
        if params is not None:
            request_params="&".join("%s=%s" % (k, v) for k,v in params.items())
            pretty_parms="\n".join("[%s]     %s=%s" % (self.name, k, v) for k,v in params.items())
            request_url='{}{}?{}'.format(self.base_url, path, request_params)
        else:
            request_url='{}{}'.format(self.base_url, path)
            pretty_parms="[{}]".format(self.name)
        
        response_file=os.path.join(self.result_dir, output)
        
        print('[{}] Request:'.format(self.name, request_url.replace(self.base_url, '<{}_BASE_URL>'.format(self.name))))
        print('{}'.format(pretty_path))
        print('{}'.format(pretty_parms))
        response = None
        if (overwrite) or not (os.path.exists(response_file)):
            response = requests.get(request_url, headers=headers)
            if response.status_code == 200:
                print('[{}] Response:'.format(self.name))
                print('[{}]   code: {}'.format(self.name, response.status_code))
                print('[{}]   mime: {}'.format(self.name, self.mime.guess_type(response_file)[0]))
                print('[{}]   file: {}'.format(self.name, response_file))
                print('[{}]   time: {}s'.format(self.name, response.elapsed.total_seconds()))
                print('[{}]    url: {}'.format(self.name, request_url))
                with open(response_file, 'wb') as filew:
                    filew.write(response.content)
            else:
                print('[{}] Request failed: {}'.format(self.name, request_url))

        print('###############')
        return ( response, response_file )
    
def main():
    client=DAPAProvider("Foo","Bar")
    print(client.name)

if __name__ == "__main__":
    main()